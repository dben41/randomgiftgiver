import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;


public class RandomGiftGiver {

	public static void main(String[] args) {
		
		//Put all parties in here
		ArrayList<String> people = new ArrayList<String>();
		people.add("Corrine");
		people.add("Mark");
		people.add("Blake");
		people.add("Collin");
		people.add("Summer");
		people.add("Daryl");
		people.add("Spencer");
		people.add("Connie");
		people.add("Mike");
		people.add("Justine");
		people.add("Josh");
		
		HashMap<String, String> assignations = new HashMap<String, String>();	
		ArrayList<Integer> used = new ArrayList<Integer>();
		
		//Make the Assignations
		for(String person: people){
			Random rand = new Random(); 
			int value = -1;
			value = rand.nextInt(11);
			//if value hasn't been used before, if assignations still need to be made, if user isn't assigned to themselves
			while(used.contains(value) && assignations.size() < 11 || value == people.indexOf(person)){
				value = rand.nextInt(11);
			}
			used.add(value);
			assignations.put(person, people.get(value));
		}
		
		 // Print Assignations to Console
	      Set set = assignations.entrySet();
	      Iterator iterator = set.iterator();
	      while(iterator.hasNext()) {
	         Map.Entry mentry = (Map.Entry)iterator.next();
	         System.out.println( mentry.getKey() + " is assigned to: " + mentry.getValue());
	      }
		
		
		

	}

}
